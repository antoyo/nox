(*
 * Copyright (C) 2018  Boucher, Antoni <bouanto@zoho.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

type lex_error = {
    pos: Reader.file_position;
    msg: string;
}

exception Error of lex_error

class lexer reader =
    object (self)
        method lex =
            match reader#peek_char with
            | ' ' | '\n' | '\r' | '\t' ->
                reader#next_char;
                Stream.slazy (fun () -> self#lex)
            | 'A' .. 'Z' | 'a' .. 'z' as char ->
                let buffer = Buffer.create 1 in
                Buffer.add_char buffer char;
                reader#next_char;
                self#ident buffer
            | '0' .. '9' as char ->
                let buffer = Buffer.create 1 in
                Buffer.add_char buffer char;
                reader#next_char;
                self#number buffer
            | '"' ->
                let buffer = Buffer.create 1 in
                reader#next_char;
                self#string buffer
            | char ->
                if reader#peek_2char = "(*" then
                    self#comment
                else (
                    let op =
                        match char with
                        | '(' -> Token.ParenOpen
                        | ')' -> Token.ParenClose
                        | ';' ->
                            if reader#peek_next_char = ';' then (
                                reader#next_char;
                                Token.SemiColon2
                            )
                            else
                                Token.SemiColon
                        | ',' -> Token.Comma
                        | '=' -> Token.Equal
                        | '-' -> Token.Minus
                        | '+' -> Token.Plus
                        | '*' -> Token.Star
                        | '<' -> Token.LesserThan
                        | '>' -> Token.GreaterThan
                        | _ ->
                            raise (Error {
                                pos = reader#position;
                                msg = "unexpected char " ^ (String.make 1 char)
                            })
                    in
                    reader#next_char;
                    Stream.iapp (Stream.ising op) (Stream.slazy (fun () -> self#lex))
                )
            | exception End_of_file ->
                Stream.sempty

        method private comment =
            match reader#peek_char with
            | '*' when reader#peek_next_char = ')' ->
                reader#next_char;
                reader#next_char;
                Stream.slazy (fun () -> self#lex)
            | _ ->
                reader#next_char;
                self#comment
            | exception End_of_file ->
                Stream.sempty

        method private ident buffer =
            match reader#peek_char with
            | 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' as char ->
                reader#next_char;
                Buffer.add_char buffer char;
                self#ident buffer
            | _ ->
                let ident_stream =
                    match Buffer.contents buffer with
                    | "else" -> Stream.ising Token.Else
                    | "extern" -> Stream.ising Token.Extern
                    | "for" -> Stream.ising Token.For
                    | "fun" -> Stream.ising Token.Fun
                    | "if" -> Stream.ising Token.If
                    | "in" -> Stream.ising Token.In
                    | "then" -> Stream.ising Token.Then
                    | "var" -> Stream.ising Token.Var
                    | id -> Stream.ising (Token.Ident id)
                in
                Stream.iapp ident_stream (Stream.slazy (fun () -> self#lex))

        method private number buffer =
            match reader#peek_char with
            (* TODO: handle errors for input like 9.12.23.34 *)
            | '0' .. '9' | '.' as char ->
                Buffer.add_char buffer char;
                reader#next_char;
                self#number buffer
            | _ ->
                let num_stream = Stream.ising (Token.Float (float_of_string (Buffer.contents buffer))) in
                Stream.iapp num_stream (Stream.slazy (fun () -> self#lex))

        method private string buffer =
            let rec read_string () =
                match reader#peek_char with
                | '"' ->
                    reader#next_char;
                    Buffer.contents buffer
                | char ->
                    reader#next_char;
                    Buffer.add_char buffer char;
                    read_string ()
            in
            Stream.ising (Token.String (read_string ()))
    end
