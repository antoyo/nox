(*
 * Copyright (C) 2018  Boucher, Antoni <bouanto@zoho.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

type file_position = {
    position_column: int;
    position_filename: string;
    position_line: int;
}

val complete_string_of_file_pos : file_position -> string
val string_of_file_pos : file_position -> string

class reader : string ->
    object
        method close : unit
        method peek_char : char
        method peek_2char : string
        method peek_next_char : char
        method next_char : unit
        method position : file_position
    end
