(*
 * Copyright (C) 2018  Boucher, Antoni <bouanto@zoho.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

type op =
    | GreaterThan
    | LesserThan
    | Minus
    | Plus
    | Times

type expr =
    | Binary of {
        op: op;
        left: expr;
        right: expr;
    }
    | Call of {
        func_name: string;
        args: expr array;
    }
    | Float of float
    | For of string * expr * expr * expr option * expr (* TODO: remove? *)
    | If of {
        cond: expr;
        if_expr: expr;
        else_expr: expr;
    }
    | Sequence of expr list
    | String of string
    | Unary of {
        op: op;
        expr: expr;
    }
    | Var of {
        var_expr: expr;
        var_name: string;
        body: expr;
    }
    | Variable of string

type proto =
    | Prototype of {
        func_name: string;
        params: string array;
    }
    | BinOpPrototype of {
        func_name: string;
        params: string array;
    }

type func =
    Function of {
        proto: proto;
        body: expr;
    }

type toplevel =
    | Def of func
    | Extern of proto

type modul = {
    declarations: toplevel list;
    filename: string;
}

val op_of_token : Token.token -> op
