(*
 * Copyright (C) 2018  Boucher, Antoni <bouanto@zoho.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Ctypes
open Llvm

open Gen
open Lexer
open Linker
open Parser
open Reader

let compile_file filename =
    let reader = new reader filename in
    try
        let lexer = new lexer reader in
        let parsr = new parsr lexer in
        let ast = parsr#parse filename in
        let generator = new generator ast in
        let filenames = generator#object_file in
        link filenames;
    with
        | Lexer.Error { pos; msg } ->
            print_endline (complete_string_of_file_pos pos ^ "Syntax error: " ^ msg ^ " at " ^
                string_of_file_pos pos);
        | Parser.Error { msg } ->
            print_endline ("Parse error: " ^ msg);
    reader#close (* FIXME: should be called before *)

let main () =
    let filename =
        if Array.length Sys.argv > 1 then
            Sys.argv.(1)
        else (
            print_endline "Missing filename";
            exit (-1)
        )
    in
    compile_file "std/std.nx";
    compile_file filename;
;;

main ()
