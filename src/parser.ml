(*
 * Copyright (C) 2018  Boucher, Antoni <bouanto@zoho.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Ast
open Token

(* TODO: add position *)
type parse_error = {
    (*pos: file_position;*)
    msg: string;
}

exception Error of parse_error

class parsr lexer =
    object (self)
        val stream = lexer#lex
        val mutable toplevels = []

        method private drop =
            Stream.junk stream

        method private eat token expected =
            if Stream.next stream = token then
                ()
            else
                raise (Error { msg = expected })

        method private ident expected =
            match Stream.peek stream with
            | Some (Token.Ident id) ->
                self#drop;
                id
            | _ ->
                raise (Error { msg = "expected identifier after " ^ expected })

        method private primary =
            match Stream.peek stream with
            | Some (Token.Float n) ->
                self#drop;
                Ast.Float n
            | Some Token.ParenOpen ->
                self#drop;
                let expr = self#expr in
                self#eat Token.ParenClose "expected ')'";
                expr
            | Some (Token.Ident id) ->
                self#drop;
                let rec parse_args accumulator stream =
                    let expr = self#expr in
                        match Stream.peek stream with
                        | token when is_end_delimiter token ->
                            expr :: accumulator
                        | _ ->
                            parse_args (expr :: accumulator) stream
                in
                let rec parse_ident id =
                    match Stream.peek stream with
                    | token when is_end_delimiter token ->
                        Ast.Variable id
                    | _->
                        let args = parse_args [] stream in
                        Ast.Call {
                            func_name = id;
                            args = Array.of_list (List.rev args);
                        }
                in
                parse_ident id
            | Some Token.If ->
                self#drop;
                let cond = self#expr in
                self#eat Token.Then "expected 'then'";
                let if_expr = self#expr in
                self#eat Token.Else "expected 'else'";
                let else_expr = self#expr in
                Ast.If {
                    cond;
                    if_expr;
                    else_expr;
                }
            | Some Token.For ->
                self#drop;
                let var_name = self#ident "for" in
                self#eat (Token.Equal) "expected '=' after for";
                let start = self#expr in
                self#eat (Token.Comma) "expected ',' after for";
                let end_val = self#expr in
                let step =
                    (match Stream.peek stream with
                    | Some (Token.Comma) ->
                        self#drop;
                        let step = self#expr in
                        Some step
                    | _ -> None
                    )
                in
                self#eat Token.In "expected 'in' after for";
                let body = self#expr in
                Ast.For (var_name, start, end_val, step, body);
            | Some Token.Var ->
                self#drop;
                let var_name = self#ident "var" in
                self#eat Token.Equal "expected '=' after variable name in 'let'";
                let var_expr = self#expr in
                self#eat Token.In "expected 'in' keyword after 'var'";
                let body = self#expr in
                Ast.Var {
                    var_name;
                    var_expr;
                    body;
                }
            | token -> raise (Error {
                (*pos = {};*)
                msg = "unknown token " ^ string_of_optional_token token ^ ", expecting an expression";
            })

        method private expr =
            self#relational_expr

        method private additive_expr =
            (* TODO: add missing operators *)
            let left = self#unary_expr in
            let rec additive_exprs left =
                match Stream.peek stream with
                | Some token when token_is_additive_op token ->
                    self#drop;
                    let right = self#unary_expr in
                    let op = op_of_token token in
                    additive_exprs (Ast.Binary {
                        op;
                        left;
                        right;
                    })
                | _ -> left
            in additive_exprs left

        method private relational_expr =
            let left = self#additive_expr in
            let rec relational_exprs left =
                match Stream.peek stream with
                | Some token when token_is_relational_op token ->
                    self#drop;
                    let right = self#additive_expr in
                    let op = op_of_token token in
                    relational_exprs (Ast.Binary {
                        op;
                        left;
                        right;
                    })
                | _ -> left
            in relational_exprs left

        method private unary_expr =
            match Stream.peek stream with
            | Some Token.Minus ->
                self#drop;
                let expr = self#unary_expr in
                Ast.Unary {
                    op = Ast.Minus;
                    expr;
                }
            | _ -> self#primary

        method private prototype =
            let rec parse_args accumulator stream =
                match Stream.peek stream with
                | Some (Token.Ident id) ->
                    self#drop;
                    parse_args (id :: accumulator) stream
                | _ -> accumulator
            in
            match Stream.peek stream with
            | Some (Token.Ident func_name) ->
                self#drop;
                let args = parse_args [] stream in
                Ast.Prototype {
                    func_name;
                    params = Array.of_list (List.rev args);
                }
            (* TODO: add support for defining new operators *)
            | _ ->
                raise (Error { msg = "expected function name in prototype" })

        method private definition =
            match Stream.peek stream with
            | Some Token.Fun ->
                self#drop;
                let proto = self#prototype in
                self#eat Token.Equal "expected '=' after prototype";
                let body = self#expr in
                Ast.Function {proto; body}
            | _ ->
                raise (Error { msg = "expected fun in function definition" })

        method private extern =
            match Stream.peek stream with
            | Some Token.Extern ->
                self#drop;
                self#prototype
            | _ ->
                raise (Error { msg = "expected extern in extern" })

        method private toplevel =
            (*(match Stream.peek stream with
            | Some token -> print_endline (Token.show_token token)
            | None -> ());*)
            let expr = self#expr in
            toplevels <- expr :: toplevels

        method parse filename =
            let rec parse acc =
                match Stream.peek stream with
                | None ->
                    List.rev acc
                | Some SemiColon2 ->
                    self#drop;
                    parse acc
                | Some token -> (
                    match token with
                    | Token.Extern ->
                        parse (Ast.Extern self#extern::acc)
                    | Token.Fun ->
                        parse (Ast.Def self#definition::acc)
                    | _ ->
                        self#toplevel;
                        parse acc
                )
            in
            let declarations = parse [] in
            let main_func = Ast.Def (Ast.Function {
                proto = Ast.Prototype {
                    func_name = "main";
                    params = [||];
                };
                body = Ast.Sequence toplevels;
                }) in
            let declarations = List.rev (main_func :: declarations) in
            {
                declarations;
                filename;
            }
    end
