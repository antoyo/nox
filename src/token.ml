(*
 * Copyright (C) 2018  Boucher, Antoni <bouanto@zoho.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

type token =
    | Comma
    | Else
    | Equal
    | Extern
    | For
    | Fun
    | GreaterThan
    | Ident of string
    | If
    | In
    | LesserThan
    | Minus
    | Float of float
    | ParenClose
    | ParenOpen
    | Plus
    | SemiColon
    | SemiColon2
    | String of string
    | Star
    | Then
    | Var
[@@deriving show]

let string_of_optional_token = function
    | None -> "<eof>"
    | Some token ->
        match token with
        | Comma -> ","
        | Else -> "else"
        | Equal -> "="
        | Extern -> "extern"
        | For -> "for"
        | Fun -> "fun"
        | GreaterThan -> ">"
        | Ident ident -> ident
        | If -> "if"
        | In -> "in"
        | LesserThan -> "<"
        | Minus -> "-"
        | Float num -> string_of_float num
        | ParenClose -> ")"
        | ParenOpen -> "("
        | Plus -> "+"
        | SemiColon -> ";"
        | SemiColon2 -> ";;"
        | String string -> "\"" ^ string ^ "\""
        | Star -> "*"
        | Then -> "then"
        | Var -> "var"

let token_is_additive_op = function
    | Minus | Plus -> true
    | _ -> false

let token_is_relational_op = function
    | GreaterThan | LesserThan -> true
    | _ -> false

let is_end_delimiter = function
    | Some (Ident _) | Some (Float _) | Some ParenOpen -> false
    | _ -> true
